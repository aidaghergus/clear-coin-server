package com.clearpay.exercise;

import com.clearpay.exercise.errorhandling.exception.NotEnoughCoins;
import com.clearpay.exercise.model.entity.Wallet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WalletOperationsTests {

	@Test
	void testDebitWallet() {
		Wallet wallet=new Wallet(null,20D);
		wallet.debit(10D);
		Assertions.assertEquals(30D,wallet.getBalance());
	}

	@Test
	void testCreditWallet(){
		Wallet wallet=new Wallet(null,20D);
		wallet.credit(10D);
		Assertions.assertEquals(10D,wallet.getBalance());
	}

	@Test
	void testCreditWalletNotEnoughCoins(){
		Assertions.assertThrows(NotEnoughCoins.class, () -> {
			Wallet wallet=new Wallet(null,20D);
			wallet.credit(25D);
		});
	}

}
