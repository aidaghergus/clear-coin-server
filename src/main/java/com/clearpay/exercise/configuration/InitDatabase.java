package com.clearpay.exercise.configuration;

import com.clearpay.exercise.model.entity.User;
import com.clearpay.exercise.model.entity.Wallet;
import com.clearpay.exercise.repository.UserRepository;
import com.clearpay.exercise.repository.WalletRepository;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitDatabase {

  private static final Logger LOGGER = LoggerFactory.getLogger(InitDatabase.class);

  @Bean
  CommandLineRunner initDatabaseWithData(UserRepository userRepository, WalletRepository walletRepository) {

    return args -> {
      userRepository.save(new User("10", "Elon Musk", "elon.musk@mail.com", Arrays.asList("607f14edd74bbe4191bf0c70","607f14edd74bbe4191bf1d71")));
      userRepository.save(new User("11", "Vitalik Buterin", "vitalikkk@mail.com", Arrays.asList("607f14edd74bbe4191bf2e72","607f14edd74bbe4191bf3f73","607f14edd74bbe4191bf3f74")));
      userRepository.save(new User("12", "Cathie Wood", "iamcathie@mail.com", Arrays.asList("607f14edd74bbe4191bf3f75")));
      userRepository.save(new User("13", "Mike Tyson", "mickeyme@mail.com", Arrays.asList("607f14edd74bbe4191bf3f76","607f14edd74bbe4191bf3f77")));

      userRepository.findAll().forEach(employee -> LOGGER.info(String.format("Preloaded %s",employee)));

      walletRepository.save(new Wallet("607f14edd74bbe4191bf0c70", 200D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf1d71", 0D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf2e72", 10D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf3f73", 25D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf3f74", 250D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf3f75", 1D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf3f76", 100.4D));
      walletRepository.save(new Wallet("607f14edd74bbe4191bf3f77", 25.5D));

      walletRepository.findAll().forEach(order -> LOGGER.info(String.format("Preloaded %s", order)));

    };
  }

}
