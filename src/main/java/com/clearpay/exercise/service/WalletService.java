package com.clearpay.exercise.service;

import com.clearpay.exercise.errorhandling.exception.WalletNotFoundException;
import com.clearpay.exercise.model.entity.Wallet;
import com.clearpay.exercise.repository.WalletRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WalletService {

  @Autowired
  private WalletRepository walletRepository;

  public List<Wallet> getAllWallets() {
    return walletRepository.findAll();
  }

  public Wallet getWalletById(String walletId){
    return walletRepository.findById(walletId).orElseThrow(WalletNotFoundException::new);
  }

  @Transactional
  public void transferCoins(Wallet from, Wallet to, Double amount) {
    from.credit(amount);
    to.debit(amount);
    walletRepository.save(from);
    walletRepository.save(to);
  }
}
