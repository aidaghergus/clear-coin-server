package com.clearpay.exercise.service;

import com.clearpay.exercise.errorhandling.exception.UserNotFoundException;
import com.clearpay.exercise.errorhandling.exception.WalletNotFoundException;
import com.clearpay.exercise.model.entity.User;
import com.clearpay.exercise.model.entity.Wallet;
import com.clearpay.exercise.repository.UserRepository;
import com.clearpay.exercise.repository.WalletRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private WalletRepository walletRepository;

  public User getUserById(String id){
    return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
  }

  public List<User> getAllUsers(){
    return userRepository.findAll();
  }

  public List<Wallet> getUserWallets(User user){
    List<Wallet> wallets=new ArrayList<>();
    user.getWallets().forEach(walletId -> wallets.add(walletRepository.findById(walletId).orElseThrow(WalletNotFoundException::new)));
    return wallets;
  }

  @Transactional
  public User openWallet(User user){
    Wallet wallet=walletRepository.save(new Wallet(null,0D));
    if(user.getWallets()==null)
      user.setWallets(new ArrayList<>());
    user.addWallet(wallet.getId());
    return userRepository.save(user);
  }

}
