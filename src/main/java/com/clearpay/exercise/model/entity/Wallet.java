package com.clearpay.exercise.model.entity;

import com.clearpay.exercise.errorhandling.exception.NotEnoughCoins;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "wallets")
public class Wallet {

  @Id
  private String id;
  private Double balance;

  public void debit(Double amount){
    this.balance+=amount;
  }

  public void credit(Double amount)
  {
    if(this.balance<amount)
      throw new NotEnoughCoins();
    this.balance-=amount;
  }
}
