package com.clearpay.exercise.model.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
public class UserDTO implements Serializable {
  public String id;
  public String name;
  public String email;
  public List<String> wallets;
}
