package com.clearpay.exercise.model.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class WalletDTO implements Serializable {
  public String id;
  public Double balance;
}
