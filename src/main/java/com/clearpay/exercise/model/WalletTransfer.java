package com.clearpay.exercise.model;

import lombok.Data;

@Data
public class WalletTransfer {
  private String from;
  private String to;
  private Double amount;

}
