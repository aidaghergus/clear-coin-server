package com.clearpay.exercise.model.mapper;

import com.clearpay.exercise.model.entity.Wallet;
import com.clearpay.exercise.model.dto.WalletDTO;
import org.mapstruct.Mapper;

@Mapper
public interface WalletMapper {
  Wallet mapToEntity(WalletDTO walletDTO);
  WalletDTO mapToDTO(Wallet wallet);
}
