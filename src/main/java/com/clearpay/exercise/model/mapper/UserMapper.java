package com.clearpay.exercise.model.mapper;

import com.clearpay.exercise.model.entity.User;
import com.clearpay.exercise.model.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
  User mapToEntity(UserDTO userDTO);
  UserDTO mapToDTO(User user);
}
