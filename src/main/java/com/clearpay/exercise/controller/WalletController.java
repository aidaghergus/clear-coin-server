package com.clearpay.exercise.controller;

import com.clearpay.exercise.model.WalletTransfer;
import com.clearpay.exercise.model.dto.WalletDTO;
import com.clearpay.exercise.model.mapper.WalletMapper;
import com.clearpay.exercise.service.WalletService;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@CrossOrigin
@RequestMapping("/wallets")
public class WalletController {

  @Autowired
  WalletService walletService;

  @GetMapping
  public List<WalletDTO> getAllWallets(){
    return walletService.getAllWallets().stream().map(wallet -> Mappers.getMapper(WalletMapper.class).mapToDTO(wallet)).collect(Collectors.toList());
  }

  @PostMapping("/transfer")
  public void transfer(@RequestBody WalletTransfer walletTransfer){
    walletService.transferCoins(walletService.getWalletById(walletTransfer.getFrom()), walletService.getWalletById(walletTransfer.getTo()), walletTransfer.getAmount());
  }
}
