package com.clearpay.exercise.controller;

import com.clearpay.exercise.model.dto.UserDTO;
import com.clearpay.exercise.model.dto.WalletDTO;
import com.clearpay.exercise.model.mapper.UserMapper;
import com.clearpay.exercise.model.mapper.WalletMapper;
import com.clearpay.exercise.service.UserService;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@CrossOrigin
@RequestMapping("/users")
public class UserController {

  @Autowired
  UserService userService;

  @GetMapping
  public List<UserDTO> getAllUser(){
    return userService.getAllUsers().stream().map(user -> Mappers.getMapper(UserMapper.class).mapToDTO(user)).collect(Collectors.toList());
  }

  @GetMapping("/wallets")
  public List<WalletDTO> getAllWalletsForUser(@RequestParam("user") String userId){
    return userService.getUserWallets(userService.getUserById(userId)).stream().map(wallet -> Mappers.getMapper(WalletMapper.class).mapToDTO(wallet)).collect(Collectors.toList());
  }

  @PostMapping("/open-wallet")
  public UserDTO openWallet(@RequestBody String userId){
    return Mappers.getMapper(UserMapper.class).mapToDTO(userService.openWallet(userService.getUserById(userId)));
  }
}
