package com.clearpay.exercise.errorhandling.handler;

import com.clearpay.exercise.errorhandling.ErrorResponse;
import com.clearpay.exercise.errorhandling.exception.NotEnoughCoins;
import com.clearpay.exercise.errorhandling.exception.UserNotFoundException;
import com.clearpay.exercise.errorhandling.exception.WalletNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HttpExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger EXCEPTION_LOGGER = LoggerFactory.getLogger(HttpExceptionHandler.class);

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<Object> handleUserNotFound(RuntimeException exception, WebRequest request) {
    EXCEPTION_LOGGER.error(exception.getMessage(), exception);
    return new ResponseEntity<>(new ErrorResponse("User not found"),  HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(WalletNotFoundException.class)
  public ResponseEntity<Object> handleWalletNotFound(RuntimeException exception, WebRequest request) {
    EXCEPTION_LOGGER.error(exception.getMessage(), exception);
    return new ResponseEntity<>(new ErrorResponse("Wallet not found"),  HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(NotEnoughCoins.class)
  public ResponseEntity<Object> handleNotEnoghCoins(RuntimeException exception, WebRequest request) {
    EXCEPTION_LOGGER.error(exception.getMessage(), exception);
    return new ResponseEntity<>(new ErrorResponse("Not enough coins in the wallet"),  HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
