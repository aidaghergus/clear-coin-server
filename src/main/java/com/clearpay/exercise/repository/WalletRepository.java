package com.clearpay.exercise.repository;

import com.clearpay.exercise.model.entity.Wallet;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WalletRepository extends MongoRepository<Wallet, String> {

}
